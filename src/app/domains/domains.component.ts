import { HttpClient } from '@angular/common/http';
import { Headers } from '@angular/http';
import { RequestOptions, Request, RequestMethod } from '@angular/http';
import { Http, ResponseContentType } from '@angular/http';
import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Component, Directive, Input, Renderer,  ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router'
import { Config } from '../../assets/config/config';
import * as $ from 'jquery';
import { DomainsService } from './domains.service';
import { Domains , Domain} from './domain';
import { ScriptLoader } from '../scriptmodel.service'
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';


@Component(
    {
        selector: 'app-root',
        templateUrl: './domains.html',
        providers: <any>[DomainsService,ScriptLoader]
    }
) 

@Injectable()
export class DomainsComponent {
    title = 'Domains';
    appconfig = null;
    host = null;
    idToDelete = 0;

    count = ""

    customerCount = 0;
    customerChange = "";
    
    currentPage = 1;
    totalPages = 0;
    all = 0;
    active = 0;
    isEmpty = false;

    @ViewChild('alertContainer') alertContainer: ElementRef;

    @ViewChild('data') content: ElementRef;
    @ViewChild('nodata') nocontent: ElementRef;
    @ViewChild('submitButton') submitButton: ElementRef;
    @ViewChild('clearSearch') clearSearch: ElementRef;

    @ViewChild('allMembersView') allMembersView: ElementRef;
    @ViewChild('newConvertsView') newConvertsView : ElementRef;
    @ViewChild('newFirstTimersView') newFirstTimersView : ElementRef;

    public modalRef: BsModalRef;

    constructor(public http: Http, private modalService: BsModalService, public activatedRoute: ActivatedRoute, private router: Router, private config: Config, private _domainsService: DomainsService, public renderer: Renderer, private _scriptLoader: ScriptLoader) {
        this._scriptLoader.loadScripts();
        this.appconfig = config;
        this.host = this.appconfig.getConfig('domains-service');
    
        this.activatedRoute.queryParams.subscribe((params: Params) => {
            let page = params['_navigate'];

            if (page == null) {
                this.currentPage = 1;
            }
            else {
                try {
                    this.currentPage = page
                }
                catch (e) {
                    this.router.navigate(['']);
                }

            }
        });

        this.getDomains(this.host + 'domains?page='+( (this.currentPage))); 
     
    }

    //private allMembers: MemberData;
    domains: Domain[] = [];
    private errorMessage: any = '';
   

    showDate(date){
        var actualDate = date.split("T")
        return actualDate[0]
    }

    
    ngOnInit() {
       //get table data
       this._scriptLoader.loadScripts();

        /*if (sessionStorage.getItem("message")) {
            this.message = sessionStorage.getItem("message")
            this.alertContainer.nativeElement.innerHTML = "<div class='alert alert-success' role='alert'>" + this.message + "</div>";
            sessionStorage.removeItem("message")
        }*/

        if (sessionStorage.getItem("count")) {
            this.alertContainer.nativeElement.innerHTML = "<div class='alert alert-success' role='alert'>" + sessionStorage.getItem("count")+ " new members added</div>";
            sessionStorage.removeItem("message")
            sessionStorage.removeItem("count")
        }

        this.activatedRoute
            .queryParams
            .subscribe(queryParams => {
                let page = queryParams['_navigate'];

                if (page == null) {
                    this.currentPage = 1;
                }
                else {
                    try {
                        this.currentPage = page
                    }
                    catch (e) {
                        this.router.navigate(['']);
                    }

                }
            });  

        this.getDomains(this.host + 'domains?page='+( (this.currentPage))); 
    }

    getDomains(url) {
        //To Do: Fetch Posts here using PostsDataService
       
        this._domainsService.getData(url)
            .subscribe(response => {
 
                if (response.code == this.appconfig.getConfig('authError')) {
                    this.router.navigate(['']);
                    sessionStorage.setItem("message", this.appconfig.getConfig('authMessage'))
                }
                
                this.domains = response.domains
                this.totalPages = response.totalPages
                this.all = response.all
                this.active =  response.totalCount
      
                 /*if (response.domains != null) {
                    //populate table
                    this.domains = response.domains,
                    error => this.errorMessage = <any>error
                    this.totalPages = response.totalPages
                }
                else{
                    this.content.nativeElement.hidden = true
                    this.renderer.setElementStyle(this.nocontent.nativeElement, 'display', 'block');
                }*/
            });
   }
    
}
