export interface Domains {
    code: number,
    all : number;
    description: string,
    currentCount : number,
    totalCount : number,
    totalPages : number,
    domains: Domain[] 
} 


export interface Domain {
    code: number,
    description: string,
    subscriber: string,
    userid: number,
    completed: boolean,
    expiry: string,
    domainName: string,
    createdAt: string
} 
