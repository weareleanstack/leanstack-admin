import { Injectable } from '@angular/core';
import { Component, Directive, Input, ViewChild, ElementRef } from '@angular/core';
import { Http, ResponseContentType } from '@angular/http';
import { ScriptLoader } from '../scriptmodel.service'
import { Config } from '../../assets/config/config';
import { DashboardService } from './dashboard.service';
import { Router } from '@angular/router'
import { CustomerOverview, Customer, Frequency } from './customerOverview';
import '../../../node_modules/chart.js/src/chart.js';

@Component(
    {
        selector: 'app-root',
        templateUrl: './app.dashboard.html',
        styleUrls: ['./app.dashboard.css'],
        providers: <any>[ScriptLoader,DashboardService]
    }
)

@Injectable()
export class DashboardComponent {
    busy: Promise<any>;
    
    customerCount = 0;
    customerChange = "";

    domainsCount = 0;
    domainsChange = "";

    financeCount = 0;
    financeChange = ""

    freqTotal = 0;

    salesStart = "XXXX,XX";
    salesEnd = "XXXX,XX";

    expensesStart = "XXXX,XX";
    expensesEnd = "XXXX,XX";

    periodSales = 0;
    periodExpenses = 0;

    customers: Customer[] = [];

    frequency: Frequency[] = [];

    salesChartLabels: Array<any> = [];

    salesChartData: Array<any> = [
        {
            data: [],
            label: null,
        }
    ];

    expensesChartLabels: Array<any> = [];

    expensesChartData: Array<any> = [
        {
            data: [],
            label: null,
        }
    ];

    public lineChartLegend: boolean = true;
    public lineChartType: string = 'line';

    public lineChartOptions: any = {
        responsive: true
    };

    public lineChartColors: Array<any> = [
        { // blue
            backgroundColor: 'rgba(0,147,221,0.2)',
            borderColor: '#2083fe',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        }]

    public offeringlineChartColors: Array<any> = [
        { // green
            backgroundColor: 'rgba(53,201,116,0.2)',
            borderColor: '#35c974',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        }]

    constructor(private http: Http, private _scriptLoader: ScriptLoader, private router: Router, private config: Config, private _dashboardService: DashboardService) { 
        this.getCustomerOverview(config.getConfig("customers-service")+"customers/overview")
        this.getDomainsOverview(config.getConfig("domains-service")+"domains/overview")
        this.getFinanceOverview(config.getConfig("finance-service") + "admin/overview")
        this.getHostingOverview(config.getConfig("hosting-service")+"overview")
    }

    ngOnInit() {
        //this._scriptLoader.loadScripts();
        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));
    }

    getReadableDate(timestamp){
        var d = new Date(timestamp);
        return d.getDay()+"/"+(d.getMonth()+1)+"/"+d.getFullYear()
    }

    getCustomerOverview(url) {
        //To Do: Fetch Posts here using PostsDataService
        this._dashboardService.getCustomersOverview(url)
            .subscribe(
                response => {

                    if (response.code == this.config.getConfig('authError')) {
                        this.router.navigate(['']);
                        sessionStorage.setItem("message", this.config.getConfig('authMessage'))
                    }

                    this.customerCount = response.customersCount;
                    this.customerChange = response.change + ""
                    this.customers = response.recentCustomers
                })
    }

    getDomainsOverview(url) {
        //To Do: Fetch Posts here using PostsDataService
        this._dashboardService.getDomainsOverview(url)
            .subscribe(
                response => {

                    if (response.code == this.config.getConfig('authError')) {
                        this.router.navigate(['']);
                        sessionStorage.setItem("message", this.config.getConfig('authMessage'))
                    }

                    this.domainsCount = response.activeDomains;
                    this.domainsChange = response.change + ""
                })
    }

    getHostingOverview(url) {
        //To Do: Fetch Posts here using PostsDataService
        this._dashboardService.getHostingOverview(url)
            .subscribe(
                response => {
                    console.log(response)

                    if (response.code == this.config.getConfig('authError')) {
                        this.router.navigate(['']);
                        sessionStorage.setItem("message", this.config.getConfig('authMessage'))
                    }

                    this.freqTotal = response.hostingsFrequency.map(item => item.noOfHosting).reduce((prev, next) => prev + next);
                    this.frequency = response.hostingsFrequency

          })
    }

    getFinanceOverview(url) {
        //To Do: Fetch Posts here using PostsDataService
        this._dashboardService.getFinanceOverview(url)
            .subscribe(
                response => {

                    if (response.code == this.config.getConfig('authError')) {
                        this.router.navigate(['']);
                        sessionStorage.setItem("message", this.config.getConfig('authMessage'))
                    }

                    this.financeCount = response.totalRevenue;
                    this.financeChange = response.change + ""

                    this.salesStart = response.salesStartMonth
                    this.salesEnd = response.salesEndMonth
                    
                    this.expensesStart = response.expensesStartMonth
                    this.expensesEnd = response.expensesEndMonth

                    var totalSales = 0;
                    if (response.sales != null && response.sales.length > 0) {
                        response.sales.forEach(x => totalSales += x.amount);
                    }

                    if(response.sales != null){
                         
                            for (var i = 0; i < response.sales.length; i++) {
                                this.salesChartData[0].data.push(response.sales[i].amount)
                            }

                            this.salesChartData[0].label = "Sales Trend"

                            for (var i = 0; i < response.sales.length; i++) {
                                this.salesChartLabels.push(response.sales[i].month)
                            }

                            let _lineChartData: Array<any> = new Array(this.salesChartData.length);
                            for (let i = 0; i < this.salesChartData.length; i++) {
                                _lineChartData[i] = { data: new Array(this.salesChartData[i].data.length), label: this.salesChartData[i].label };
                                for (let j = 0; j < this.salesChartData[i].data.length; j++) {
                                    _lineChartData[i].data[j] = this.salesChartData[i].data[j];
                                }
                            }


                            this.salesChartData = _lineChartData;
                    }

                    this.periodSales = totalSales

                    var totalExpenses = 0;
                    if (response.expenses != null && response.expenses.length > 0) {
                        response.expenses.forEach(x => totalExpenses += x.amount);
                    }

                    if (response.expenses != null) {

                        for (var i = 0; i < response.expenses.length; i++) {
                            this.expensesChartData[0].data.push(response.expenses[i].amount)
                        }

                        this.expensesChartData[0].label = "Expenses Trend"

                        for (var i = 0; i < response.expenses.length; i++) {
                            this.expensesChartLabels.push(response.expenses[i].month)
                        }

                        let _lineChartDataE: Array<any> = new Array(this.expensesChartData.length);
                        for (let i = 0; i < this.expensesChartData.length; i++) {
                            _lineChartDataE[i] = { data: new Array(this.expensesChartData[i].data.length), label: this.expensesChartData[i].label };
                            for (let j = 0; j < this.expensesChartData[i].data.length; j++) {
                                _lineChartDataE[i].data[j] = this.expensesChartData[i].data[j];
                            }
                        }

                        this.expensesChartData = _lineChartDataE;
                    }

                    this.periodExpenses = totalExpenses
                })
    }
}
