import { Injectable } from "@angular/core";
//import { MemberOverview } from './memberOverview';
import { CustomerOverview, DomainsOverview, FinanceOverview, HostingOverview } from './customerOverview';
import { Http, Response, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Config } from '../../assets/config/config';
import { Headers } from '@angular/http';
import { RequestOptions, Request, RequestMethod } from '@angular/http';

@Injectable()
export class DashboardService {
    appconfig = null;

    constructor(private http: Http, private config: Config) {
        this.appconfig = config;
    }

    getCustomersOverview(url): Observable<CustomerOverview> {
        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));

        let options = new RequestOptions({
            method: RequestMethod.Get,
            headers: new Headers({ 'sessionId': storage.sessionId }),
            responseType: ResponseContentType.Json
        });

        return this.http.get(url, options)
            .pipe(map(this.extractData))
    }

    getDomainsOverview(url): Observable<DomainsOverview> {
        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));

        let options = new RequestOptions({
            method: RequestMethod.Get,
            headers: new Headers({ 'sessionId': storage.sessionId }),
            responseType: ResponseContentType.Json
        });

        return this.http.get(url, options)
            .pipe(map(this.extractData))
    }

    getHostingOverview(url): Observable<HostingOverview> {
        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));

        let options = new RequestOptions({
            method: RequestMethod.Get,
            headers: new Headers({ 'sessionId': storage.sessionId }),
            responseType: ResponseContentType.Json
        });

        return this.http.get(url, options)
            .pipe(map(this.extractData))
    }

    getFinanceOverview(url): Observable<FinanceOverview> {
        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));

        let options = new RequestOptions({
            method: RequestMethod.Get,
            headers: new Headers({ 'sessionId': storage.sessionId }),
            responseType: ResponseContentType.Json
        });

        return this.http.get(url, options)
            .pipe(map(this.extractData))
    }

    private extractData(res: Response) {
        let body = res.json();
        return body;
    }

    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }
}

