export interface CustomerOverview {
    code: number;
    description: string;
    customersCount: number,
    customersLastCount: number,
    change: number,
    recentCustomers: Customer[]
}

export interface Customer {
    id: number,
    isActive: boolean,
    createDate: string,
    lastUpdateDate: string,
    firstname: string,
    lastName: string,
    username: string,
    email: string,
    phone: string,
    password: string,
    pleskId: number
} 

export interface DomainsOverview {
    code: number,
    description: string,
    activeDomains: number,
    activeDomainsLastWeek: number,
    change: number
}

export interface FinanceOverview {
    code: number,
    description: string,
    totalRevenue: number,
    totalRevenueLastWeek: number,
    expensesStartMonth:string,
    expensesEndMonth: string,
    salesStartMonth: string,
    salesEndMonth: string,
    change: number,
    sales : Sales[],
    expenses : Expenses[]
}

export interface Sales {
    month : string,
    amount : number
}

export interface Expenses {
    month: string,
    amount: number
}

export interface HostingOverview {
    code: number,
    description: string,
    currentHostingCount: number,
    lastWeekHostingCount: number,
    change: number,
    hostingsFrequency: Frequency[]
}

export interface Frequency {
    plan: string,
    noOfHosting: number
}