import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { ForgotComponent } from './passwordForgot/forgot.component';
import { ChangeComponent } from './passwordChange/change.component';
import { CreateCustomerComponent } from './customer/customer.component';
import { CustomersComponent} from './customers/customers.component';
import { ProfileComponent } from './profile/app.profile.component';
import { AdminComponent} from './admins/admin.component';
import { DomainsComponent} from './domains/domains.component';
import { SubscriptionsComponent } from './subscriptions/subscriptions.component'
import { TLDComponent } from './tld/tld.component';

const routes: Routes = [
    {
        path: '',
        component: LoginComponent
    },
    {
        path: 'dashboard',
        component: DashboardComponent,
        data: {
            title: 'Control Center'
        }
    },
    {
        path: 'reset/initiate',
        component: ForgotComponent
    },
    {
        path: 'password/change',
        component: ChangeComponent
    },
    {
        path: 'customers',
        component: CustomersComponent,
        data: {
            title: 'Customers'
        }
    },
    {
        path: 'customers/create',
        component: CreateCustomerComponent,
        data: {
            title: 'Add a Customer'
        }
    },
    {
        path: 'customer/profile/:id',
        component: ProfileComponent,
        data: {
            title: 'Customer Profile'
        }
    },
    {
        path: 'admins',
        component: AdminComponent,
        data: {
            title: 'Manage Admins'
        }
    },
    {
        path: 'domains',
        component: DomainsComponent,
        data: {
            title: 'Reseller Club Domains'
        }
    },
    {
        path: 'subscriptions',
        component: SubscriptionsComponent,
        data: {
            title: 'Plesk Subscriptions'
        }
    },
    {
        path: 'tld-management',
        component: TLDComponent,
        data: {
            title: 'TLD Manager'
        }
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: []
})
export class AppRoutingModule {
}