import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, APP_INITIALIZER } from '@angular/core';
import { SideComponent } from './sidemenu/sidemenu.component';
import { TopComponent } from './topmenu/topmenu.component';
import { ForgotComponent } from './passwordForgot/forgot.component';
import { ChangeComponent } from './passwordChange/change.component';
import { Config } from '../assets/config/config';
import { ChartsModule } from 'ng2-charts';
import { Router, ActivatedRoute, Params } from '@angular/router'
import { CreateCustomerComponent } from './customer/customer.component';
import { CustomersComponent, AlertContentComponent} from './customers/customers.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ProfileComponent } from './profile/app.profile.component';
import { AdminComponent} from './admins/admin.component';
import {DomainsComponent} from './domains/domains.component'
import { SubscriptionsComponent } from './subscriptions/subscriptions.component'
import { TLDComponent } from './tld/tld.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    SideComponent,
    TopComponent,
    ForgotComponent,
    ChangeComponent,
    CreateCustomerComponent,
    CustomersComponent,
    AlertContentComponent,
    ProfileComponent,
    AdminComponent,
    DomainsComponent,
    SubscriptionsComponent,
    TLDComponent
  ],
  imports: [ 
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    ChartsModule,
    HttpModule,
    ModalModule.forRoot()
  ],
  entryComponents: [AlertContentComponent],
  exports: [
    CustomersComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],  
  providers: [
    Config,
    { provide: APP_INITIALIZER, useFactory: (config: Config) => () => config.load(), deps: [Config], multi: true }
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
  constructor(private router: Router, public activatedRoute: ActivatedRoute) {
    var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));
   
    if (storage) {
      if (storage.changePassword) {
        if (storage.changePassword == true) {
          this.router.navigate(['password/change']);
        }
      }
    }
    else {
      this.activatedRoute.queryParams.subscribe((params: Params) => {
        let maskedId = params['key'];
        if (maskedId == "") {
          this.router.navigate(['']);
        }
        else {
          //alert(maskedId)
        }
      });

    }

  }
}
