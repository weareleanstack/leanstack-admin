import { Injectable } from "@angular/core";
//import { MemberOverview } from './memberOverview';
import { Http, Response, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Config } from '../../assets/config/config';
import { Headers } from '@angular/http';
import { RequestOptions, Request, RequestMethod } from '@angular/http';
import { Customer, Domains, Hostings, Orders, Activities, Plans, Countries} from './profile';

@Injectable()
export class ProfileService {
    appconfig = null;

    constructor(private http: Http, private config: Config) {
        this.appconfig = config;
    }

    getCustomerData(url): Observable<Customer> {
        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));

        let options = new RequestOptions({
            method: RequestMethod.Get,
            headers: new Headers({ 'sessionId': storage.sessionId }),
            responseType: ResponseContentType.Json
        });

        return this.http.get(url, options)
            .pipe(map(this.extractData))
    }

    getCustomerDomains(url): Observable<Domains> {
        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));

        let options = new RequestOptions({
            method: RequestMethod.Get,
            headers: new Headers({ 'sessionId': storage.sessionId }),
            responseType: ResponseContentType.Json
        });

        return this.http.get(url, options)
            .pipe(map(this.extractData))
    }

    getCustomerActivities(url): Observable<Activities> {
        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));

        let options = new RequestOptions({
            method: RequestMethod.Get,
            headers: new Headers({ 'sessionId': storage.sessionId }),
            responseType: ResponseContentType.Json
        });

        return this.http.get(url, options)
            .pipe(map(this.extractData))
    }

    getPlans(url): Observable<Plans> {
        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));

        let options = new RequestOptions({
            method: RequestMethod.Get,
            headers: new Headers({ 'sessionId': storage.sessionId }),
            responseType: ResponseContentType.Json
        });

        return this.http.get(url, options)
            .pipe(map(this.extractData))
    }

    getCountries(url): Observable<Countries> {
        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));

        let options = new RequestOptions({
            method: RequestMethod.Get,
            headers: new Headers({ 'sessionId': storage.sessionId }),
            responseType: ResponseContentType.Json
        });

        return this.http.get(url, options)
            .pipe(map(this.extractData))
    }

    getCustomerHosting(url): Observable<Hostings> {
        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));

        let options = new RequestOptions({
            method: RequestMethod.Get,
            headers: new Headers({ 'sessionId': storage.sessionId }),
            responseType: ResponseContentType.Json
        });

        return this.http.get(url, options)
            .pipe(map(this.extractData))
    }

    getCustomerOrders(url): Observable<Orders> {
        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));

        let options = new RequestOptions({
            method: RequestMethod.Get,
            headers: new Headers({ 'sessionId': storage.sessionId }),
            responseType: ResponseContentType.Json
        });

        return this.http.get(url, options)
            .pipe(map(this.extractData))
    }

    private extractData(res: Response) {
        let body = res.json();
        return body;
    }

    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }
}

