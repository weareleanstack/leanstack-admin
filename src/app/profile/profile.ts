export interface Customer {
    code : number,
    description : string,
    id: number,
    created : string,
    firstName: string;
    phone: string;
    lastName: string;
    city: string,
    address: string,
    pleskid: string,
    state: string,
    email: string,
    country: string,
    postcode : string
} 

export interface Plans {
    code : number,
    description : string,
    data : Plan[]
} 

export interface Plan {
    monthlyCost : number;
    yearlyCost:  number;
    availableMonthly : boolean;
    plan : string;
    planId : number;
    noOfDatabases : number;
    serverSize : number;
    isUnlimited : boolean;
    noOfDomains : number;
    description : string;
    discount : number;
}

export interface Countries{
    code : number,
    description : string,
    countries : Country[]
} 

export interface Country {
    id: number,
    name : string,
    phonecode : string
}

export interface Domains {
    code : number,
    description : string,
    domains : Domain[]
} 

export interface Domain {
    id : number,
    createDate : string,
    domainName : string,
    expiryDate : string
}

export interface Hostings {
    code : number,
    description : string,
    hostings : Hosting[]
} 

export interface Hosting {
    id : number,
    createDate : string,
    domainName : string,
    expiryDate : string,
    planName : string
}

export interface Orders {
    code : number,
    description : string,
    orders : Order[]
} 

export interface Order {
    id : number,
    date : string,
    amount : string,
    status : string,
    type : string,
    description : string
}

export interface Activities {
    code : number,
    description : string,
    activities : Activity[]
} 

export interface Activity {
    date : string,
    activity : string,
    activitySummary : string
}