import { Component, Directive, Input, ViewChild, ElementRef } from '@angular/core';
import { Http, ResponseContentType } from '@angular/http';
import { ScriptLoader } from '../scriptmodel.service'
import { Config } from '../../assets/config/config';
import { ProfileService } from  './profile.service';
import '../../../node_modules/chart.js/src/chart.js';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Injectable, Renderer } from '@angular/core';
import { Customer, Domains, Domain, Hosting, Order, Activity, Plan, Country} from './profile';
import { Headers } from '@angular/http';
import { RequestOptions, Request, RequestMethod } from '@angular/http';
import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';

@Component(
    {
        selector: 'app-root',
        templateUrl: './profile.html',
        providers: <any>[ScriptLoader,ProfileService]
    }
)

@Injectable()
export class ProfileComponent {
    busy: Promise<any>;
    
    id: number;
    host = null;
    sub: any;
    customer: Customer;
    domains: Domain[] = [];
    plans: Plan[] = [];
    plan: Plan;
    hostings: Hosting[] = []
    orders: Order[] = []
    activities: Activity[] = []
    countries: Country[] = []

    messageFlag = 0;
    responseMessage = ""
    available = ""

    firstname = "---";
    phone = "234";
    lastname = "---";
    city = "---";
    address = "---";
    state = "---";
    email = "---";
    country = "---";
    postcode = "";
    pleskid = "";

    @ViewChild('submitProfileButton') submitProfileButton: ElementRef;
    @ViewChild('submitContactButton') submitContactButton: ElementRef;
    @ViewChild('domainAlertContainer') domainAlertContainer: ElementRef;
    @ViewChild('submitDomainButton') submitDomainButton: ElementRef;
    @ViewChild('submitHostingButton') submitHostingButton: ElementRef;
    @ViewChild('hostingAlertContainer') hostingAlertContainer: ElementRef;
    
    constructor(public http: Http, private route: ActivatedRoute,  public renderer: Renderer, private router: Router, private config: Config, private _profileService: ProfileService, private _scriptLoader: ScriptLoader) {
    }

   

    ngOnInit() {
        this._scriptLoader.loadScripts();

        this.sub = this.route.params.subscribe(params => {
            this.id = +params['id']; // (+) converts string 'id' to a number

            this.getCustomer(this.config.getConfig('customers-service') + 'customers/'+this.id); 
            this.getDomains(this.config.getConfig('domains-service') + 'domains/customer/'+this.id)
            this.getOrders(this.config.getConfig('finance-service') + 'orders/customer/'+this.id)
            this.getActivities(this.config.getConfig('activities-service')+'activity/customer/'+this.id)
            this.getPlans(this.config.getConfig('hosting-service')+'/plans')
            this.getCountries(this.config.getConfig('customers-service')+'/countries');
        });
    }

    getCustomer(url) {
        //To Do: Fetch Posts here using PostsDataService
        this._profileService.getCustomerData(url)
          .subscribe(response => {
              this.customer = response
              console.log(this.customer);
  
              if (response.code == this.config.getConfig('authError')) {
                  this.router.navigate(['']);
                  sessionStorage.setItem("message", this.config.getConfig('authMessage'))
              }
  
              if(response.code == 0){
  
                  /*if(this.member.gender == "male"){
                      this.avatar = "assets/img/male-avatar.png"
                  }
                  else{
                      this.avatar = "assets/img/female-avatar.png"
                  }*/
                  
                  this.firstname = this.customer.firstName;
                  this.lastname = this.customer.lastName
                  this.email = this.customer.email
                  this.phone = this.customer.phone
                  this.address = this.customer.address
                  this.city = this.customer.city
                  this.address = this.customer.address
                  this.state = this.customer.state
                  this.country = this.customer.country
                  this.postcode = this.customer.postcode
                  this.pleskid = this.customer.pleskid

                  this.getHostings(this.config.getConfig('hosting-service') + 'hosting/customer/'+this.id+"/"+this.pleskid)
         
              }
              else{
  
              }
        });
      }

      Rand(min: number, max: number) {
        return (Math.random() * (max - min + 1) | 0) + min;
    }  

      onSubmitDomain(domainForm){
          //pull domain details


          //get domain and tld
          let body = new Array();
          var domainName = domainForm.value.domain
          var tokens = domainName.split(".",2)
          if(tokens.length < 2){
            this.domainAlertContainer.nativeElement.innerHTML = "<div class='alert alert-danger' role='alert'>Please provide domain name and tld</div>"
            return;  
          }

          body['domainName'] =  tokens[0];
          body['tld'] = tokens[1];
          body['customerId'] = this.id;
          body['invoiceId'] = "LNST"+this.Rand(9000,123000);
          body['customerName'] = this.firstname + ' ' +this.lastname
          body['status'] = true;
          body['duration'] = domainForm.value.duration
          body['paymentRef'] = domainForm.value.reference
          body['paymentChannel'] = domainForm.value.channel
          body['purpose'] = "DOMAIN PROVISIONING";

          if(domainForm.value.whois == null || domainForm.value.whois == undefined || domainForm.value.whois == false)
              body['whois'] = false
           else
              body['whois'] = true

          var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));
       
          let options = new RequestOptions({
              method: RequestMethod.Get,
              headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded', 'sessionId': storage.sessionId  }),
              responseType: ResponseContentType.Json
          });

         
          const req = new Request(options);
  
          this.submitDomainButton.nativeElement.innerHTML = '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>'
          this.submitDomainButton.nativeElement.disabled = true;
   
          //search for domain
          return this.http.get(this.config.getConfig("domains-service")+'search/'+tokens[0]+"/"+tokens[1],  options)
              .subscribe(response => {
                  console.log(response);
                  
                  if (response.status) {
                      var resp: any = response;
                      if (resp._body.code == 0) {
                        this.domainAlertContainer.nativeElement.innerHTML = "<div class='alert alert-success' role='alert'>Domain name is available, placing order..</div>"
                        options.method = RequestMethod.Post;

                        body['amount'] = (resp._body.price - (resp._body.currentDiscount / 100 * resp._body.price) )  * domainForm.value.duration

                        //place domain order and mark as paid
                        return this.http.post(this.config.getConfig("finance-service")+'domain/order', Object.keys(body).map(key => key + '=' + body[key]).join('&'),  options)
                            .subscribe(response => {
                                console.log(response);
                                
                                if (response.status) {
                                    var resp: any = response;
                                    if (resp._body.code == 0) {
                                        this.domainAlertContainer.nativeElement.innerHTML = "<div class='alert alert-success' role='alert'>Domain order placed, provisioning domain...</div>"
                                        
                                        let update = new Array();
                                        update['customerId'] = this.id;
                                        update['orderId'] = resp._body.description

                                        //provision domain
                                        return this.http.post(this.config.getConfig("domains-service")+'domain/provision', Object.keys(update).map(key => key + '=' + update[key]).join('&'),  options)
                                        .subscribe(response => {
                                            console.log(response);
                                            this.submitDomainButton.nativeElement.innerHTML = 'Submit'
                                            this.submitDomainButton.nativeElement.disabled = false;
                                            
                                            if (response.status) {
                                                var resp: any = response;
                                                if (resp._body.code == 0) {
                                                    this.domainAlertContainer.nativeElement.innerHTML = "<div class='alert alert-success' role='alert'>Domain Provisioned Successfully</div>"
                                                    return;
                                                }
                                                else {
                                                    
                                                    this.domainAlertContainer.nativeElement.innerHTML = "<div class='alert alert-danger' role='alert'>Could not provision domain</div>"
                                                    return;
                                                }
                                            }
                            
                                        });
                                    }
                                    else {
                                        this.submitDomainButton.nativeElement.innerHTML = 'Submit'
                                        this.submitDomainButton.nativeElement.disabled = false;
                                        this.domainAlertContainer.nativeElement.innerHTML = "<div class='alert alert-danger' role='alert'>Could not place domain order</div>"
                                        return;
                                    }
                                }
                
                            });
                    }
                      else {
                        this.submitDomainButton.nativeElement.innerHTML = 'Submit'
                        this.submitDomainButton.nativeElement.disabled = false;
                        this.domainAlertContainer.nativeElement.innerHTML = "<div class='alert alert-danger' role='alert'>"+resp._body.description+"</div>"
                        return;
                      }
                  }
  
              });
      }

     onSubmitHosting(hostingForm){
        //pull domain details
        //get domain and tld
        let body = new Array();
        var domainName = hostingForm.value.domain
        
        body['domainName'] =  domainName;
        body['customerId'] = this.id;
        body['status'] = true;

        this.plan = this.plans[hostingForm.value.plan];

        var tokens = hostingForm.value.duration.split("-",2)
        body['span'] = tokens[1]
        body['duration'] = tokens[0]

        body['planId'] = this.plan.planId
        
        if (domainName.indexOf("www.") !=-1) {
            this.hostingAlertContainer.nativeElement.innerHTML = "<div class='alert alert-danger' role='alert'>Please remove www.</div>"
            return;  
        }

        if(body['span'] == 'YEARLY'){
           body['amount'] = body['duration'] * this.plan.yearlyCost;

         }
        else{
            if(this.plan.availableMonthly == false){
                this.hostingAlertContainer.nativeElement.innerHTML = "<div class='alert alert-danger' role='alert'>Monthly billing is not available for this hosting</div>"
                return;  
            }
           body['amount'] = body['duration'] * this.plan.monthlyCost;
        }

       
        
        body['amount'] = (body['amount'] - (this.plan.discount / 100 * body['amount']) )
        body['invoiceId'] = "LNST"+this.Rand(9000,123000);
        body['paymentChannel'] = hostingForm.value.channel
        body['paymentRef'] = hostingForm.value.reference
        body['customerName'] = this.firstname + ' ' +this.lastname
        body['purpose'] = "HOSTING PROVISIONING";

        var tokens = hostingForm.value.duration.split("-",2)
        

        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));
     
        let options = new RequestOptions({
            method: RequestMethod.Get,
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded', 'sessionId': storage.sessionId  }),
            responseType: ResponseContentType.Json
        });

        const req = new Request(options);

        this.submitHostingButton.nativeElement.innerHTML = '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>'
        this.submitHostingButton.nativeElement.disabled = true;
 
        //search for hosting
        return this.http.get(this.config.getConfig("hosting-service")+'hosting/check-existence/'+body['domainName'].toLowerCase(),  options)
            .subscribe(response => {
                console.log(response);
                
                if (response.status) {
                    var resp: any = response;
                    if (resp._body.code == 0) {
                      this.hostingAlertContainer.nativeElement.innerHTML = "<div class='alert alert-success' role='alert'>Domain name is available for hosting, placing order..</div>"
                      options.method = RequestMethod.Post;

                      //body['amount'] = (resp._body.price - (resp._body.currentDiscount / 100 * resp._body.price) )  * domainForm.value.duration
                      

                      //place domain order and mark as paid
                      return this.http.post(this.config.getConfig("finance-service")+'hosting/order', Object.keys(body).map(key => key + '=' + body[key]).join('&'),  options)
                          .subscribe(response => {
                              console.log(response);
                              
                              if (response.status) {
                                  var resp: any = response;
                                  if (resp._body.code == 0) {
                                      this.hostingAlertContainer.nativeElement.innerHTML = "<div class='alert alert-success' role='alert'>Hosting order placed, setting up hosting...</div>"
                                      
                                      let update = new Array();
                                      update['customerId'] = this.id;
                                      update['orderId'] = resp._body.description
                                      update['pleskId'] = this.pleskid

                                      //provision domain
                                      return this.http.post(this.config.getConfig("hosting-service")+'hosting/provision', Object.keys(update).map(key => key + '=' + update[key]).join('&'),  options)
                                      .subscribe(response => {
                                          console.log(response);
                                          this.submitHostingButton.nativeElement.innerHTML = 'Submit'
                                          this.submitHostingButton.nativeElement.disabled = false;
                                          
                                          if (response.status) {
                                              var resp: any = response;
                                              if (resp._body.code == 0) {
                                                  this.hostingAlertContainer.nativeElement.innerHTML = "<div class='alert alert-success' role='alert'>Hosting Provisioned Successfully</div>"
                                                  return;
                                              }
                                              else {
                                                  
                                                  this.hostingAlertContainer.nativeElement.innerHTML = "<div class='alert alert-danger' role='alert'>Could not provision hosting</div>"
                                                  return;
                                              }
                                          }
                          
                                      });
                                  }
                                  else {
                                      this.submitHostingButton.nativeElement.innerHTML = 'Submit'
                                      this.submitHostingButton.nativeElement.disabled = false;
                                      this.hostingAlertContainer.nativeElement.innerHTML = "<div class='alert alert-danger' role='alert'>Could not place hosting order</div>"
                                      return;
                                  }
                              }
              
                          });
                  }
                    else {
                      this.submitHostingButton.nativeElement.innerHTML = 'Submit'
                      this.submitHostingButton.nativeElement.disabled = false;
                      this.hostingAlertContainer.nativeElement.innerHTML = "<div class='alert alert-danger' role='alert'>"+resp._body.description+"</div>"
                      return;
                    }
                }

            });
    }
    
      getDomains(url) {
        //To Do: Fetch Posts here using PostsDataService
        this._profileService.getCustomerDomains(url)
          .subscribe(response => {
              this.domains = response.domains
              console.log(this.domains);
  
              if (response.code == this.config.getConfig('authError')) {
                  this.router.navigate(['']);
                  sessionStorage.setItem("message", this.config.getConfig('authMessage'))
              }
  
              if(response.code == 0){
  
                  /*if(this.member.gender == "male"){
                      this.avatar = "assets/img/male-avatar.png"
                  }
                  else{
                      this.avatar = "assets/img/female-avatar.png"
                  }*/
  
              }
              else{
  
              }
        });
      }

      getCountries(url) {
        //To Do: Fetch Posts here using PostsDataService
        this._profileService.getCountries(url)
          .subscribe(response => {
              this.countries = response.countries
              console.log(this.countries);
  
              if (response.code == this.config.getConfig('authError')) {
                  this.router.navigate(['']);
                  sessionStorage.setItem("message", this.config.getConfig('authMessage'))
              }
  
              if(response.code == 0){
              }
              else{
  
              }
        });
      }

      getHostings(url) {
        //To Do: Fetch Posts here using PostsDataService
        this._profileService.getCustomerHosting(url)
          .subscribe(response => {
              this.hostings = response.hostings
              console.log(this.domains);
  
              if (response.code == this.config.getConfig('authError')) {
                  this.router.navigate(['']);
                  sessionStorage.setItem("message", this.config.getConfig('authMessage'))
              }
  
              if(response.code == 0){
  
                  /*if(this.member.gender == "male"){
                      this.avatar = "assets/img/male-avatar.png"
                  }
                  else{
                      this.avatar = "assets/img/female-avatar.png"
                  }*/
  
              }
              else{
  
              }
        });
      }

      getActivities(url) {
        //To Do: Fetch Posts here using PostsDataService
        this._profileService.getCustomerActivities(url)
          .subscribe(response => {
              this.activities = response.activities
              console.log(this.domains);
  
              if (response.code == this.config.getConfig('authError')) {
                  this.router.navigate(['']);
                  sessionStorage.setItem("message", this.config.getConfig('authMessage'))
              }
  
              if(response.code == 0){
  
                  /*if(this.member.gender == "male"){
                      this.avatar = "assets/img/male-avatar.png"
                  }
                  else{
                      this.avatar = "assets/img/female-avatar.png"
                  }*/
  
              }
              else{
  
              }
        });
      }

      getOrders(url) {
        //To Do: Fetch Posts here using PostsDataService
        this._profileService.getCustomerOrders(url)
          .subscribe(response => {
              this.orders = response.orders
              console.log(this.domains);
  
              if (response.code == this.config.getConfig('authError')) {
                  this.router.navigate(['']);
                  sessionStorage.setItem("message", this.config.getConfig('authMessage'))
              }
  
              if(response.code == 0){
  
                  /*if(this.member.gender == "male"){
                      this.avatar = "assets/img/male-avatar.png"
                  }
                  else{
                      this.avatar = "assets/img/female-avatar.png"
                  }*/
  
              }
              else{
  
              }
        });
      }

      getPlans(url) {
        //To Do: Fetch Posts here using PostsDataService
        this._profileService.getPlans(url)
          .subscribe(response => {
              this.plans = response.data
              console.log(this.plans);
  
              if (response.code == this.config.getConfig('authError')) {
                  this.router.navigate(['']);
                  sessionStorage.setItem("message", this.config.getConfig('authMessage'))
              }
  
              if(response.code == 0){}
              else{
  
              }
        });
      }

    showDate(date){
        var actualDate = date.split("T")
        return actualDate[0]
     }

     showTime(date){
        var actualDate = date.split("T")
        var time = actualDate[1].split("+")
        return time[0]
     }
   
     transform(word){
        if(word == 'PAID')
            return 'success'
        else
            return 'warning'
     }
   
      profileSubmit(updateProfileForm) {
        let body = new Array();
        body['firstname'] = updateProfileForm.value.firstname
        body['lastname'] = updateProfileForm.value.lastname
        body['email'] = updateProfileForm.value.email
        body['phone'] = updateProfileForm.value.phone
        body['password'] = ""

        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));
       
        let options = new RequestOptions({
            method: RequestMethod.Put,
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded', 'sessionId': storage.sessionId  }),
            responseType: ResponseContentType.Json
        });

        const req = new Request(options);

        this.submitProfileButton.nativeElement.innerHTML = '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>'
        this.submitProfileButton.nativeElement.disabled = true;
 
        return this.http.post(this.config.getConfig("customers-service")+'customers/'+this.id, Object.keys(body).map(key => key + '=' + body[key]).join('&'), options)
            .subscribe(response => {
                console.log(response);
                this.submitProfileButton.nativeElement.innerHTML = 'Submit'
                this.submitProfileButton.nativeElement.disabled = false;
                
                if (response.status) {
                    var resp: any = response;
                    if (resp._body.code == 0) {
                        //this.alertContainer.nativeElement.innerHTML = resp._body.description;
                        setTimeout(function () {
                            this.messageFlag = 1;
                            this.responseMessage = "Profile Updated Successfully"
                        }.bind(this), 500);
                    }
                    else {
                        setTimeout(function () {
                            this.messageFlag = 2;
                            this.responseMessage = resp._body.description
                        }.bind(this), 500);
                    }
                }

            });
    }

    contactSubmit(updateContactForm) {
        let contact = new Array()
        contact['address'] = updateContactForm.value.address
        contact['state'] = updateContactForm.value.state
        contact['postcode'] = updateContactForm.value.postcode
        contact['country'] = updateContactForm.value.country
        contact['city'] = updateContactForm.value.city
        contact['customerId'] = this.id

        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));

        let options = new RequestOptions({
            method: RequestMethod.Post,
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded', 'sessionId': storage.sessionId }),
            responseType: ResponseContentType.Json
        });

        const req = new Request(options);

        this.submitContactButton.nativeElement.innerHTML = '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>'
        this.submitContactButton.nativeElement.disabled = true;
 
                     return this.http.post(this.config.getConfig("customers-service")+'contact', Object.keys(contact).map(key => key + '=' + contact[key]).join('&'), options)
                        .subscribe(response => {
                            this.submitContactButton.nativeElement.innerHTML = 'Submit'
                            this.submitContactButton.nativeElement.disabled = false;
            
                            console.log(response);
                            if (response.status) {
                                var resp: any = response;
                                if (resp._body.code != 0) {
                                    this.messageFlag = 2;
                                    this.responseMessage = resp._body.description
                                }
                                else {
                                    this.messageFlag = 1;
                                    this.responseMessage = "Contact updated Successfully"
                               }
                            }
            
                        });
                   
    }
    
}
