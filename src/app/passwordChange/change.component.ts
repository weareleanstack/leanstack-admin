import { HttpClient } from '@angular/common/http';
import { Headers } from '@angular/http';
import { RequestOptions, Request, RequestMethod } from '@angular/http';
import { Http, ResponseContentType } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Component, Directive, Input, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router'
import { Config } from '../../assets/config/config';

@Component(
    {
        selector: 'app-root',
        templateUrl: './app.change.html',
        styleUrls: ['./app.change.css']
    }
)

@Injectable()
export class ChangeComponent {
    title = 'Leanstack Administrator';


    @ViewChild('alertContainer') alertContainer: ElementRef;
    @ViewChild('submitButton') submitButton: ElementRef;

    constructor(public http: Http, private router: Router, private config: Config) {
    }

    onSubmit(contactForm) {
        let body = new Array();
        body['currentPassword'] = contactForm.value.currentPassword
        body["newPassword"] = contactForm.value.newPassword;
        body["confirmPassword"] = contactForm.value.confirmPassword;

        if (contactForm.value.newPassword != contactForm.value.confirmPassword){
            this.alertContainer.nativeElement.innerHTML = "<div class='alert alert-danger' role='alert'>Incorrect username or password</div>";
            setTimeout(function () {
                this.alertContainer.nativeElement.innerHTML = "";
            }.bind(this), 3000);
        }

        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));
      
        let options = new RequestOptions({
            method: RequestMethod.Post,
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded', 'sessionId' : storage.sessionId}),
            responseType: ResponseContentType.Json
        });

        const req = new Request(options);

        this.submitButton.nativeElement.innerHTML = '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>'
        this.submitButton.nativeElement.disabled = true;

        return this.http.post(this.config.getConfig("admin-service") +'admin/password/change', Object.keys(body).map(key => key + '=' + body[key]).join('&'), options)
            .subscribe(response => {
                this.submitButton.nativeElement.innerHTML = 'Submit'
                this.submitButton.nativeElement.disabled = false;

                if (response.status) {
                    var resp: any = response;
                    if (resp._body.code != 0) {
                        this.alertContainer.nativeElement.innerHTML = "<div class='alert alert-danger' role='alert'>"+resp._body.description+"</div>";
                        setTimeout(function () {
                            this.alertContainer.nativeElement.innerHTML = "";
                        }.bind(this), 3000);
                    }
                    else {
                        //reset data in session storage
                       storage.changePassword = false;
                       sessionStorage.setItem("sessionDetail", JSON.stringify(storage));
                       this.router.navigate(['./dashboard']);
                    }
                }

            });
    }
}