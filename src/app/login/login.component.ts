import { HttpClient } from '@angular/common/http';
import { Headers } from '@angular/http';
import { RequestOptions, Request, RequestMethod } from '@angular/http';
import { Http, ResponseContentType } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Component, Directive, Input, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router'
import { Config } from '../../assets/config/config';

@Component(
    {
        selector: 'app-root',
        templateUrl: './app.login.html',
        styleUrls: ['./app.login.css'],
    }
)

@Injectable()
export class LoginComponent {
    title = 'Leanstack Administrator';


    @ViewChild('alertContainer') alertContainer: ElementRef;
    @ViewChild('loginButton') loginButton: ElementRef;

    constructor(public http: Http, private router: Router, private config: Config) {
    }

    onSubmit(contactForm) {
        let body = new Array();
        body['email'] = contactForm.value.email
        body["password"] = contactForm.value.password;

        let options = new RequestOptions({
            method: RequestMethod.Post,
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }),
            responseType: ResponseContentType.Json
        });

        const req = new Request(options);

        this.loginButton.nativeElement.innerHTML = '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>'
        this.loginButton.nativeElement.disabled = true;
 
        return this.http.post(this.config.getConfig("admin-service")+'authenticate', Object.keys(body).map(key => key + '=' + body[key]).join('&'), options)
            .subscribe(response => {
                this.loginButton.nativeElement.innerHTML = 'Login'
                this.loginButton.nativeElement.disabled = false;

                if (response.status) {
                    var resp: any = response;
                    if (resp._body.code != 0) {
                        this.alertContainer.nativeElement.innerHTML = "<div class='alert alert-danger' role='alert'>Incorrect username or password</div>";
                        setTimeout(function () {
                            this.alertContainer.nativeElement.innerHTML = "";
                        }.bind(this), 3000);
                    }
                    else {
                        sessionStorage.setItem("sessionDetail", JSON.stringify(resp._body));
                        if (resp._body.changePassword == true) {
                            this.router.navigate(['./password/change']);
                        }
                        else {
                            this.router.navigate(['./dashboard']);
                        }
                    }
                }

            });
    }
}