import { HttpClient } from '@angular/common/http';
import { Headers } from '@angular/http';
import { RequestOptions, Request, RequestMethod } from '@angular/http';
import { Http, ResponseContentType } from '@angular/http';
import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Component, Directive, Input, Renderer,  ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router'
import { Config } from '../../assets/config/config';
import * as $ from 'jquery';
import { AdminService } from './admin.service';
import { administrator, roles, role } from './admin';
import { ScriptLoader } from '../scriptmodel.service'
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';


@Component(
    {
        selector: 'app-root',
        templateUrl: './admins.html',
        providers: <any>[AdminService, ScriptLoader]
    }
) 

@Injectable()
export class AdminComponent {
    title = 'Admins';
    toDeleteName = ""
    appconfig = null;
    idToEdit = 0;
    idToDelete = 0;
    host = null;

    admins: administrator[] = [];
    private adminToEdit : administrator;
    private adminToDelete : administrator;
    roles: role[] = [];

    @ViewChild('submitButton') submitButton: ElementRef;
    @ViewChild('createButton') createButton: ElementRef;
    @ViewChild('deleteButton') deleteButton: ElementRef;
    @ViewChild('alertContainer') alertContainer: ElementRef;
    
    constructor(public http: Http, private modalService: BsModalService, public activatedRoute: ActivatedRoute, private router: Router, private config: Config, private _adminService: AdminService, public renderer: Renderer, private _scriptLoader: ScriptLoader) {
        this._scriptLoader.loadScripts();
        this.appconfig = config;
        this.host = this.appconfig.getConfig('admin-service');

        this.getAdmins(this.host + 'admin/portal-users'); 
        this.getRoles(this.host+ 'admin/roles');
    }

    ngOnInit() {
       //get table data
       this._scriptLoader.loadScripts();

    }

    getRoles(url) {
      
        this._adminService.getRoles(url)
            .subscribe(response => {

                if(response.code == this.appconfig.getConfig("authError")){
                    this.router.navigate(['./dashboard']);
                }
 
                if (response.roles != null) {
                    //populate table
                    this.roles = response.roles
                }
               
            });
   }

    getAdmins(url) {
      
        this._adminService.getAdmins(url)
            .subscribe(response => {

                if(response.code == this.appconfig.getConfig("authError")){
                    this.router.navigate(['./dashboard']);
                }
 
                if (response.administrators != null) {
                    //populate table
                    this.admins = response.administrators
                    //error => this.errorMessage = <any>error
                }
               
            });
     }

     setEdit(id) {
         this.idToEdit = id;
     }

     setDelete(id) {
         this.idToDelete = id;
         this.toDeleteName = this.admins[this.idToDelete].name
     }

     doDelete() {
        let body = new Array();
        
        this.adminToDelete = this.admins[this.idToDelete]

        body['id'] = Number(this.adminToDelete.id)

        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));
  
        let options = new RequestOptions({
            method: RequestMethod.Delete,
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' , 'sessionId': storage.sessionId}),
            responseType: ResponseContentType.Json
        });

        const req = new Request(options);

        this.deleteButton.nativeElement.innerHTML = '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>'
        this.deleteButton.nativeElement.disabled = true;
 
        return this.http.delete(this.config.getConfig("admin-service")+'admin/portal-user/'+body['id'], options)
            .subscribe(response => {
                this.deleteButton.nativeElement.innerHTML = 'Submit Change'
                this.deleteButton.nativeElement.disabled = false;
                
                if (response.status) {
                    var resp: any = response;
                    if (resp._body.code == 0) {
                        //reload
                        location.reload();
                    }
                    else {
                        //error occured
                    }
                }

            });
    }

     doSubmit(createForm) {
        let body = new Array();
       
        body['fullname'] = createForm.value.fullname
        body['email'] = createForm.value.email
        body['phone'] = createForm.value.phone
        body['roleId'] = Number(createForm.value.role)
        body['password'] = "password";
        
        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));
  
        let options = new RequestOptions({
            method: RequestMethod.Post,
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' , 'sessionId': storage.sessionId}),
            responseType: ResponseContentType.Json
        });

        const req = new Request(options);

        this.createButton.nativeElement.innerHTML = '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>'
        this.createButton.nativeElement.disabled = true;
 
        return this.http.post(this.config.getConfig("admin-service")+'admin/create/portal-user', Object.keys(body).map(key => key + '=' + body[key]).join('&'), options)
            .subscribe(response => {
                this.createButton.nativeElement.innerHTML = 'Create Admin'
                this.createButton.nativeElement.disabled = false;
                
                if (response.status) {
                    var resp: any = response;
                    if (resp._body.code == 0) {
                       
                        //reload
                        location.reload();
                    }
                    else {
                        //error occured
                    }
                }

            });
    }

     onSubmit(updateForm) {
        let body = new Array();
        body['roleId'] = Number(updateForm.value.role)
        

        this.adminToEdit = this.admins[this.idToEdit]

        body['id'] = Number(this.adminToEdit.id)
        body['fullname'] = this.adminToEdit.name
        body['phone'] = this.adminToEdit.phone

        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));
  
        let options = new RequestOptions({
            method: RequestMethod.Put,
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' , 'sessionId': storage.sessionId}),
            responseType: ResponseContentType.Json
        });

        const req = new Request(options);

        this.submitButton.nativeElement.innerHTML = '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>'
        this.submitButton.nativeElement.disabled = true;
 
        return this.http.put(this.config.getConfig("admin-service")+'admin/edit/portal-user', Object.keys(body).map(key => key + '=' + body[key]).join('&'), options)
            .subscribe(response => {
                this.submitButton.nativeElement.innerHTML = 'Submit'
                this.submitButton.nativeElement.disabled = false;
                
                if (response.status) {
                    var resp: any = response;
                    if (resp._body.code == 0) {
                       
                        //reload
                        location.reload();
                    }
                    else {
                        //error occured
                    }
                }

            });
    }

}

