export interface Admins {
    administrators: administrator[],
    description: string,
    code : number
} 

export interface roles {
    roles: role[],
    description: string,
    code : number
} 

export interface administrator{
    code : number,
    id: number,
    phone: string,
    email: string,
    name: string,
    role : role
} 

export interface role {
    code: number,
    message: string,
    roleId: number,
    roleName: string,
    permissions: string[] 
}
