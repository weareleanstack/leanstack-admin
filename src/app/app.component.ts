import { HttpClient } from '@angular/common/http';
import { Headers } from '@angular/http';
import { RequestOptions, Request, RequestMethod } from '@angular/http';
import { Http, ResponseContentType } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import  {map} from 'rxjs/operators';
import { Component, Directive, Input, ViewChild, ElementRef } from '@angular/core';
import * as  $ from 'jquery';

@Component(
  {
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
  }
)

@Injectable()
export class AppComponent {
  toggle() {
    var $document = $(document),
      $page = $('.o-page'),
      $sidebar = $(".js-page-sidebar"), // page sidebar itself
      $sidebarToggle = $(".js-sidebar-toggle"); // sidebar toggle icon

    //alert('tooogle')
    $page.toggleClass('is-sidebar-open');
    return false;
  }
}
