import { HttpClient } from '@angular/common/http';
import { Headers } from '@angular/http';
import { RequestOptions, Request, RequestMethod } from '@angular/http';
import { Http, ResponseContentType } from '@angular/http';
import { Injectable } from '@angular/core'; 
import { ScriptLoader } from '../scriptmodel.service'
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Component, Directive, Input, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router'
import { Config } from '../../assets/config/config';
import { CustomerService } from  './customer.service';
import { Country} from './country';

@Component(
    {
        selector: 'app-root',
        templateUrl: './customer.html',
        styleUrls: ['./app.customer.css'],
        providers: <any>[ScriptLoader,CustomerService]
    }
)

@Injectable()
export class CreateCustomerComponent {
    
    title = 'Create Leanstack Customer';
    @ViewChild('alertContainer') alertContainer: ElementRef;
    @ViewChild('submitButton') submitButton: ElementRef;

    countries: Country[] = []

    constructor(public http: Http, private router: Router, private config: Config, private _customerService: CustomerService, private _scriptLoader: ScriptLoader) {
    }


    ngOnInit() {
        this._scriptLoader.loadScripts();
        this.getCountries(this.config.getConfig('customers-service')+'/countries');
    }

    getCountries(url) {
        //To Do: Fetch Posts here using PostsDataService
        this._customerService.getCountries(url)
          .subscribe(response => {
              this.countries = response.countries
       
              if (response.code == this.config.getConfig('authError')) {
                  this.router.navigate(['']);
                  sessionStorage.setItem("message", this.config.getConfig('authMessage'))
              }
  
              if(response.code == 0){
              }
              else{
  
              }
        });
      }

    onSubmit(createForm) {
        
        alert("here");
        let body = new Array();
        body['firstname'] = createForm.value.firstname
        body['lastname'] = createForm.value.lastname
        body['email'] = createForm.value.email
        body['username'] = createForm.value.username
        body['phone'] = createForm.value.phone
        body["password"] = createForm.value.password;

        //if(createForm.value.password.conta)
        //this.alertContainer.nativeElement.innerHTML = resp._body.description;

        if(createForm.value.password.includes("%")){
            this.alertContainer.nativeElement.innerHTML = "<div class='alert alert-danger' role='alert'>Password cannot contain %</div>"
            return;
        }

        let contact = new Array()
        contact['address'] = createForm.value.address
        contact['state'] = createForm.value.state
        contact['postcode'] = createForm.value.postcode
        contact['country'] = createForm.value.country
        contact['city'] = createForm.value.city

        if(contact['country'] == null || contact['country'] == '') {
            contact['country'] = 'NG';
        }

        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));

        let options = new RequestOptions({
            method: RequestMethod.Post,
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' , 'sessionId': storage.sessionId}),
            responseType: ResponseContentType.Json
        });

        const req = new Request(options);

        this.submitButton.nativeElement.innerHTML = '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>'
        this.submitButton.nativeElement.disabled = true;
 
        return this.http.post(this.config.getConfig("customers-service")+'create', Object.keys(body).map(key => key + '=' + body[key]).join('&'), options)
            .subscribe(response => {
                if (response.status) {
                    var resp: any = response;
                    if (resp._body.code != 0) {
                        this.submitButton.nativeElement.innerHTML = 'Submit'
                        this.submitButton.nativeElement.disabled = false;
        
                        this.alertContainer.nativeElement.innerHTML = "<div class='alert alert-success' role='alert'>"+resp._body.description+"</div>";
                        setTimeout(function () {
                            this.alertContainer.nativeElement.innerHTML = "";
                        }.bind(this), 3000);
                    }
                    else {
                        contact['customerId'] = resp._body.customerId
                        return this.http.post(this.config.getConfig("customers-service")+'contact', Object.keys(contact).map(key => key + '=' + contact[key]).join('&'), options)
                        .subscribe(response => {
                            this.submitButton.nativeElement.innerHTML = 'Submit'
                            this.submitButton.nativeElement.disabled = false;
            
                            if (response.status) {
                                var resp: any = response;
                                if (resp._body.code != 0) {
                                    this.alertContainer.nativeElement.innerHTML = resp._body.description;
                                    setTimeout(function () {
                                        this.alertContainer.nativeElement.innerHTML = "";
                                    }.bind(this), 1000);
                                }
                                else {
                                    sessionStorage.setItem("message", "Customer Added Successfully");
                                    this.router.navigate(['/customers']);
                                }
                            }
            
                        });
                    }
                }

            });
    }
}