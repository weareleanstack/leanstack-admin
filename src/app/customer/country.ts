export interface Countries{
    code : number,
    description : string,
    countries : Country[]
} 

export interface Country {
    id: number,
    name : string,
    phonecode : string
}