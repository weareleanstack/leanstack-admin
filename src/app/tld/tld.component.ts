import { HttpClient } from '@angular/common/http';
import { Headers } from '@angular/http';
import { RequestOptions, Request, RequestMethod } from '@angular/http';
import { Http, ResponseContentType } from '@angular/http';
import { Injectable } from '@angular/core'; 
import { ScriptLoader } from '../scriptmodel.service'
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Component, Directive, Input, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router'
import { Config } from '../../assets/config/config';
import { TldService } from './tld.service';
import { Tld, TldData} from './tld';


@Component(
    {
        selector: 'app-root',
        templateUrl: './tld.html',
        providers: <any>[ScriptLoader,TldService]
    }
)

@Injectable()
export class TLDComponent {
    
    title = 'TLD Manager';
    tldCount = 0;
    activeTlds = 0;
    host = null;
    appconfig = null;
    @ViewChild('alertContainer') alertContainer: ElementRef;
    @ViewChild('editContainer') editContainer: ElementRef;
    @ViewChild('submitButton') submitButton: ElementRef;
    @ViewChild('editButton') editButton: ElementRef;
    @ViewChild('disableButton') disableButton: ElementRef;
    @ViewChild('disableContainer') disableContainer: ElementRef;
    tlds: Tld[] = [];
    tldEdit: Tld = null;

   
    constructor(public http: Http, private router: Router, private config: Config, private _tldService: TldService, private _scriptLoader: ScriptLoader) {
        this._scriptLoader.loadScripts();
        this.appconfig = config;
        this.host = this.appconfig.getConfig('domains-service');

        this.getTlds(this.host + 'tlds/all'); 
    }

    getTlds(url) {
        //To Do: Fetch Posts here using PostsDataService
        if(this.submitButton != undefined){
            this.submitButton.nativeElement.innerHTML = '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>'
            this.submitButton.nativeElement.disabled = true;
        }
      
        this._tldService.getTlds(url)
            .subscribe(response => {
 
                if (response.allTlds != null) {
                    //populate table
                    this.tlds = response.tldData
                    this.tldEdit = this.tlds[0]
                    this.activeTlds = response.activeTlds
                    this.tldCount = response.allTlds
                }
                else{
                    //this.content.nativeElement.hidden = true
                    //this.renderer.setElementStyle(this.nocontent.nativeElement, 'display', 'block');
                }
            });
   }

   edit(id){
       this.tldEdit = this.tlds[id];
   }

    ngOnInit() {
        this._scriptLoader.loadScripts();
    }

    disable(){
        this.disableButton.nativeElement.innerHTML = '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>'
        this.disableButton.nativeElement.disabled = true;

        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));

        let options = new RequestOptions({
            method: RequestMethod.Delete,
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded', 'sessionId': storage.sessionId }),
            responseType: ResponseContentType.Json
        });
 
        return this.http.delete(this.config.getConfig("domains-service")+'remove/'+this.tldEdit.tld,  options)
            .subscribe(response => {
                if (response.status) {
                    var resp: any = response;
                    if (resp._body.code == 0) {
                        this.disableButton.nativeElement.innerHTML = 'Submit'
                        this.disableButton.nativeElement.disabled = false;
        
                        this.disableContainer.nativeElement.innerHTML = "<div class='alert alert-success' role='alert'>Tld Disabled Successfully</div>";
                        setTimeout(function () {
                            location.reload();
                        }.bind(this), 2000);
                    }
                    else {
                        this.disableButton.nativeElement.disabled = false;
                        this.disableButton.nativeElement.innerHTML = 'Add'
                        this.disableContainer.nativeElement.innerHTML = "<div class='alert alert-danger' role='alert'>"+resp._body.description+"</div>";
                    }
                }
            });
    }

    onSubmitEdit(editForm) {
        let body = new Array();
        body['tld'] = editForm.value.tld
        body['dollarPrice'] = editForm.value.dollarPrice
        body['price'] = editForm.value.price
        body['discount'] = editForm.value.discount
       
        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));

         let options = new RequestOptions({
            method: RequestMethod.Post,
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded', 'sessionId': storage.sessionId }),
            responseType: ResponseContentType.Json
        });

        const req = new Request(options);

        this.editButton.nativeElement.innerHTML = '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>'
        this.editButton.nativeElement.disabled = true;
 
        return this.http.post(this.config.getConfig("domains-service")+'tld/add', Object.keys(body).map(key => key + '=' + body[key]).join('&'), options)
            .subscribe(response => {
                if (response.status) {
                    var resp: any = response;
                    if (resp._body.code == 0) {
                        this.editButton.nativeElement.innerHTML = 'Submit'
                        this.editButton.nativeElement.disabled = false;
        
                        this.editContainer.nativeElement.innerHTML = "<div class='alert alert-success' role='alert'>Tld Added Successfully</div>";
                        setTimeout(function () {
                            this.editContainer.nativeElement.innerHTML = "";
                        }.bind(this), 3000);
                    }
                    else {
                        this.editButton.nativeElement.disabled = false;
                        this.editButton.nativeElement.innerHTML = 'Add'
                        this.editContainer.nativeElement.innerHTML = "<div class='alert alert-danger' role='alert'>"+resp._body.description+"</div>";
                    }
                }

            });
    }

    onSubmit(createForm) {
        let body = new Array();
        body['tld'] = createForm.value.tld
        body['dollarPrice'] = createForm.value.dollarPrice
        body['price'] = createForm.value.price
        body['discount'] = createForm.value.discount
       
        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));

         let options = new RequestOptions({
            method: RequestMethod.Post,
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded', 'sessionId': storage.sessionId }),
            responseType: ResponseContentType.Json
        });

        const req = new Request(options);

        this.submitButton.nativeElement.innerHTML = '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>'
        this.submitButton.nativeElement.disabled = true;
 
        return this.http.post(this.config.getConfig("domains-service")+'tld/add', Object.keys(body).map(key => key + '=' + body[key]).join('&'), options)
            .subscribe(response => {
                if (response.status) {
                    var resp: any = response;
                    if (resp._body.code == 0) {
                        this.submitButton.nativeElement.innerHTML = 'Submit'
                        this.submitButton.nativeElement.disabled = false;
        
                        this.alertContainer.nativeElement.innerHTML = "<div class='alert alert-success' role='alert'>Tld Added Successfully</div>";
                        setTimeout(function () {
                            this.alertContainer.nativeElement.innerHTML = "";
                        }.bind(this), 3000);
                    }
                    else {
                        this.submitButton.nativeElement.disabled = false;
                        this.submitButton.nativeElement.innerHTML = 'Add'
                        this.alertContainer.nativeElement.innerHTML = "<div class='alert alert-danger' role='alert'>"+resp._body.description+"</div>";
                    }
                }

            });
    }
}