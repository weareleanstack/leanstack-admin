export interface TldData {
    code: number,
    description: string,
    allTlds : number,
    activeTlds : number,
    tldData: Tld[] 
} 


export interface Tld {
    price : number,
    tld : string,
    currentDiscount: number,
    dollarPrice : number
    
} 