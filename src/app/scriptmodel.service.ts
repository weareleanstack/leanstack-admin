import { Injectable } from '@angular/core';

@Injectable()
export class ScriptLoader {

    constructor() {
        
    }

    loadScripts() {
        let url = "neat.min.js";
        if (!this.isMyScriptLoaded(url)) {
            let node = document.createElement('script');
            node.src = '../assets/js/neat.min.js';
            node.type = 'text/javascript';
            node.async = true;
            node.charset = 'utf-8';

            node.onload = function () {
            };

            document.body.appendChild(node);
        }

        
    }

    isMyScriptLoaded(url) {

        var scripts = document.getElementsByTagName('script');
        for (var i = scripts.length; i--;) {
            var res = scripts[i].src.split("/");
            if (res[res.length - 1] === url) {
                return true;
            }
        }
        return false;
    }
}