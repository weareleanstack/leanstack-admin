export interface Domains {
    code: number,
    all : number;
    description: string,
    activeCount: number,
    currentCount : number,
    totalCount : number,
    totalPages : number,
    domains: Domain[] 
} 


export interface Domain {
    code: number,
    description: string,
    subscriber: string,
    userid: number,
    completed: boolean,
    planid: number,
    duration: number,
    span: string,
    expiry: string,
    domainName: string,
    createdAt: string
} 
