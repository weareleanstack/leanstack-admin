import { Injectable } from '@angular/core';
import { Component, Directive, Input, ViewChild, ElementRef } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Title } from "@angular/platform-browser";
import { map } from 'rxjs/operators';
import { AppComponent } from '../app.component'
import { ScriptLoader } from '../scriptmodel.service'

@Component(
    {
        selector: 'top-menu',
        templateUrl: './topmenu.component.html',
        providers: <any>[ScriptLoader]

    }
)

@Injectable()
export class TopComponent {
    title = null;

    constructor(private _titleService: Title, public activatedRoute: ActivatedRoute, private router: Router, private _appComponent: AppComponent, private _scriptLoader: ScriptLoader) {
        activatedRoute.data.pipe(map(data => data.title)).subscribe(x => this._titleService.setTitle(x));

        this.title = this.activatedRoute.snapshot.data['title'];

        //log out users without session id
        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));
        if (!storage){
            sessionStorage.setItem("message", "Session Timed out, kindly login again")
            this.router.navigate(['']);
        }

    }

    ngOnInit() {
        //get table data
        this.loadScripts();
    }

    toggle() {
        this._appComponent.toggle();
    }

    loadScripts() {
        this._scriptLoader.loadScripts();
    }
}
