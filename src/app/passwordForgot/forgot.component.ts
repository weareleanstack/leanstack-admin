import { HttpClient } from '@angular/common/http';
import { Headers } from '@angular/http';
import { RequestOptions, Request, RequestMethod } from '@angular/http';
import { Http, ResponseContentType } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Component, Directive, Input, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router'
import { Config } from '../../assets/config/config';

@Component(
    {
        selector: 'app-root',
        templateUrl: './app.forgot.html',
        styleUrls: ['./app.forgot.css']
    }
)

@Injectable()
export class ForgotComponent {
    title = 'Leanstack Administrator';
   
    @ViewChild('alertContainer') alertContainer: ElementRef;
    @ViewChild('submitButton') submitButton: ElementRef;
    @ViewChild('successContainer') successContainer: ElementRef;
    @ViewChild('posterContainer') posterContainer: ElementRef;

    constructor(public http: Http, private router: Router, private config: Config) {
    }

    onSubmit(requestResetForm) {
        let body = new Array();
        body['email'] = requestResetForm.value.email

        let options = new RequestOptions({
            method: RequestMethod.Post,
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }),
            responseType: ResponseContentType.Json
        });

        const req = new Request(options);

        this.submitButton.nativeElement.innerHTML = '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>'
        this.submitButton.nativeElement.disabled = true;

        return this.http.post(this.config.getConfig("admin-service")+'admin/password/reset', Object.keys(body).map(key => key + '=' + body[key]).join('&'), options)
            .subscribe(response => {
                this.submitButton.nativeElement.innerHTML = 'Submit'
                this.submitButton.nativeElement.disabled = false;

                if (response.status) {
                    var resp: any = response;
                    if (resp._body.code != 0) {
                        this.alertContainer.nativeElement.innerHTML = "<div class='alert alert-danger' role='alert'>" + resp._body.description+"</div>";
                        setTimeout(function () {
                            this.alertContainer.nativeElement.innerHTML = "";
                        }.bind(this), 3000);
                    }
                    else {
                        this.posterContainer.nativeElement.innerHTML = "";
                        this.posterContainer.nativeElement.innerHTML = "<div class='c-note u-mb-small'><span class='c-note__icon' ><i class='feather icon-info'> </i></span><p> An email containing password reset instructions has been sent to your inbox.</p></div>";
                        
                    }
                }

            });
    }
}