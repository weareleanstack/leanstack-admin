import { HttpClient } from '@angular/common/http';
import { Headers } from '@angular/http';
import { RequestOptions, Request, RequestMethod } from '@angular/http';
import { Http, ResponseContentType } from '@angular/http';
import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Component, Directive, Input, Renderer,  ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router'
import { Config } from '../../assets/config/config';
import * as $ from 'jquery';
import { CustomersService } from './customers.service';
import { Customer , CustomerData} from './customers';
import { ScriptLoader } from '../scriptmodel.service'
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal';


@Component(
    {
        selector: 'app-root',
        templateUrl: './app.customers.html',
        styleUrls: ['./app.customers.css'],
        providers: <any>[CustomersService,ScriptLoader]
    }
) 

@Injectable()
export class CustomersComponent {
    title = 'Members';
    appconfig = null;
    host = null;
    idToDelete = 0;

    count = ""

    customerCount = 0;
    customerChange = "";
    
    currentPage = 1;
    totalPages = 0;
    isEmpty = false;

    @ViewChild('alertContainer') alertContainer: ElementRef;

    @ViewChild('data') content: ElementRef;
    @ViewChild('nodata') nocontent: ElementRef;
    @ViewChild('submitButton') submitButton: ElementRef;
    @ViewChild('clearSearch') clearSearch: ElementRef;

    @ViewChild('allMembersView') allMembersView: ElementRef;
    @ViewChild('newConvertsView') newConvertsView : ElementRef;
    @ViewChild('newFirstTimersView') newFirstTimersView : ElementRef;

    public modalRef: BsModalRef;

    constructor(public http: Http, private modalService: BsModalService, public activatedRoute: ActivatedRoute, private router: Router, private config: Config, private _customersService: CustomersService, public renderer: Renderer, private _scriptLoader: ScriptLoader) {
        this._scriptLoader.loadScripts();
        this.appconfig = config;
        this.host = this.appconfig.getConfig('customers-service');
    
        this.activatedRoute.queryParams.subscribe((params: Params) => {
            let page = params['_navigate'];

            if (page == null) {
                this.currentPage = 1;
            }
            else {
                try {
                    this.currentPage = page
                }
                catch (e) {
                    this.router.navigate(['']);
                }

            }
        });

        this.getCustomers(this.host + 'customers?page='+( (this.currentPage - 1))); 
        this.getCustomerOverview(config.getConfig("customers-service")+"customers/overview")
        //this.getStats(this.host + 'members/statistics')
    }

    //private allMembers: MemberData;
    customers: Customer[] = [];
    private errorMessage: any = '';
   


    
    ngOnInit() {
       //get table data
       this._scriptLoader.loadScripts();

        if (sessionStorage.getItem("message")) {
            this.message = sessionStorage.getItem("message")
            this.alertContainer.nativeElement.innerHTML = "<div class='alert alert-success' role='alert'>" + this.message + "</div>";
            sessionStorage.removeItem("message")
        }

        if (sessionStorage.getItem("count")) {
            this.alertContainer.nativeElement.innerHTML = "<div class='alert alert-success' role='alert'>" + sessionStorage.getItem("count")+ " new members added</div>";
            sessionStorage.removeItem("message")
            sessionStorage.removeItem("count")
        }

        this.activatedRoute
            .queryParams
            .subscribe(queryParams => {
                let page = queryParams['_navigate'];

                if (page == null) {
                    this.currentPage = 1;
                }
                else {
                    try {
                        this.currentPage = page
                    }
                    catch (e) {
                        this.router.navigate(['']);
                    }

                }
            });  

        this.getCustomers(this.host + 'customers?page=' + ((this.currentPage - 1))); 
    }

    deleteCustomer(id) {
        for(var i = 0; i < this.customers.length; i++){
            if (this.customers[i].id == id){
                this.customers.splice(i,1);
                break;
            }
        }

        let host: string = this.appconfig.getConfig('customers-service');

        var storage = JSON.parse(sessionStorage.getItem("sessionDetail"));

        let options = new RequestOptions({
            method: RequestMethod.Delete,
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded', 'sessionId': storage.sessionId }),
            responseType: ResponseContentType.Json
        });

        return this.http.post(host + 'customers/' + id, null , options)
            .subscribe(response => {
                if (response.status) {
                    var resp: any = response;
                    if (resp._body.code != 0) {
                    }
                    else {
                        sessionStorage.setItem("message", "Customers Deleted Successfully")
                        location.reload();
                    }
                }
            });
    }

    clear(){
        this.ngOnInit();
    }

    private message = ""

    openModal(id) {
        this.idToDelete = id;
        this.modalRef = this.modalService.show(AlertContentComponent, { initialState: { message: id } });
    }

    showDate(date){
        var actualDate = date.split("T")
        return actualDate[0]
    }
    
    getCustomers(url) {
        //To Do: Fetch Posts here using PostsDataService
        if(this.submitButton != undefined){
            this.submitButton.nativeElement.innerHTML = '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>'
            this.submitButton.nativeElement.disabled = true;
        }
      
        this._customersService.getData(url)
            .subscribe(response => {
 
                if (this.submitButton != undefined) {
                    this.submitButton.nativeElement.innerHTML = 'Search'
                    this.submitButton.nativeElement.disabled = false;
                }

                if (response.code == this.appconfig.getConfig('authError')) {
                    this.router.navigate(['']);
                    sessionStorage.setItem("message", this.appconfig.getConfig('authMessage'))
                }
                
            
                /*if ((url.indexOf("query") >= 0)){
                    this.renderer.setElementStyle(this.clearSearch.nativeElement, 'display', 'block');
                }
                else{
                    this.renderer.setElementStyle(this.clearSearch.nativeElement, 'display', 'none');
                }*/

               /* if (response.customers == null && (url.indexOf("query") >= 0)) {
                    this.customers = []
                    return;
                }*/

                if (response.customers != null) {
                    //populate table
                    this.customers = response.customers,
                    error => this.errorMessage = <any>error
                    this.totalPages = response.totalPages
                }
                else{
                    this.content.nativeElement.hidden = true
                    this.renderer.setElementStyle(this.nocontent.nativeElement, 'display', 'block');
                }
            });
   }

   getCustomerOverview(url) {
    //To Do: Fetch Posts here using PostsDataService
    this._customersService.getCustomersOverview(url)
        .subscribe(
            response => {

                if (response.code == this.config.getConfig('authError')) {
                    this.router.navigate(['']);
                    sessionStorage.setItem("message", this.config.getConfig('authMessage'))
                }

                this.customerCount = response.customersCount;
                this.customerChange = response.change + ""
                this.customers = response.recentCustomers
            })
}

  /*onSubmit(searchForm) {
        let query = searchForm.value.query;

        if(query == "" || query == null){
            this.getPosts(this.host + 'members/get')
        }
        else{
            this.getPosts(this.host + 'members/get?query=' + query)
        }
        
    } */
}

@Component({
    selector: 'app-root',
    templateUrl: './alert.html',
    providers: <any>[CustomersComponent, CustomersService, ScriptLoader]
})

@Injectable()
export class AlertContentComponent implements OnInit {

    @Input() message: string;
    @ViewChild('deleteButton') deleteButton: ElementRef;
   
    constructor(public bsModalRef: BsModalRef, private customersComponent: CustomersComponent) { }

    ngOnInit() {
    }

    clear() {
        this.bsModalRef.hide();
    }

    deleteCustomer() {
        this.customersComponent.deleteCustomer(this.message);
        this.deleteButton.nativeElement.innerHTML = '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>'
    }
}

