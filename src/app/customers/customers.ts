export interface CustomerData {
    code: number,
    description: string,
    currentCount : number,
    totalCount : number,
    totalPages : number,
    customers: Customer[] 
} 


export interface Customer {
    code : number,
    description : string,
    id: number,
    created : string,
    firstname: string;
    phone: string;
    lastname: string;
    city: string,
    address: string,
    state: string,
    email: string
    country: string
} 

export interface CustomerOverview {
    code: number;
    description: string;
    customersCount: number,
    customersLastCount: number,
    change: number,
    recentCustomers: Customer[]
}