$('#myTable').DataTable();

$(".addDomain").click(function(){
  $('#addDomain').modal({
    backdrop: 'static',
    keyboard: false
  });
});

$(".addHosting").click(function(){
  $('#addHosting').modal({
    backdrop: 'static',
    keyboard: false
  });
});

$(".addRole").click(function(){
  $('#addRole').modal({
    backdrop: 'static',
    keyboard: false
  });
});

$(".editUserRole").click(function(){
  $('#editUserRole').modal({
    backdrop: 'static',
    keyboard: false
  });
});

$(".delUser").click(function(){
  $('#delUser').modal({
    backdrop: 'static',
    keyboard: false
  });
});

$(".markPaid").click(function(){
  $('#markPaid').modal({
    backdrop: 'static',
    keyboard: false
  });
});