import { Inject, Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
 
@Injectable()
export class Config {

    private config: Object = null;
    private env: Object = null;

    constructor(private http: Http) {

    }

    /**
     * Use to get the data found in the second file (config file)
     */
    public getConfig(key: any) {
        return this.config[key];
    }

    /**
     * Use to get the data found in the first file (env file)
     */
    public getEnv(key: any) {
        return this.env[key];
    }

    /**
     * This method:
     *   a) Loads "env.json" to get the current working environment (e.g.: 'production', 'development')
     *   b) Loads "config.[env].json" to get all env's variables (e.g.: 'config.development.json')
     */
    public load() {
        return new Promise((resolve, reject) => {
            this.http.get('../assets/config/env.json')
            .pipe(map(res => res.json()))
            .subscribe((envResponse) => {
                this.env = envResponse;
                let request: any = null;

                switch (envResponse.env) {
                    case 'production': {
                        this.http.get('../assets/config/config.' + envResponse.env + '.json')
                            .pipe(map(res => res.json()))
                            .subscribe((responseData) => {
                                this.config = responseData;
                                resolve(true);
                            });
                    } break;

                    case 'staging': {
                        this.http.get('../assets/config/config.' + envResponse.env + '.json')
                            .pipe(map(res => res.json()))
                            .subscribe((responseData) => {
                                this.config = responseData;
                                resolve(true);
                            });
                    } break;

                    case 'development': {
                        this.http.get('../assets/config/config.' + envResponse.env + '.json')
                            .pipe(map(res => res.json()))
                            .subscribe((responseData) => {
                                this.config = responseData;
                                resolve(true);
                            });
                    } break;

                    case 'default': {
                        console.error('Environment file is not set or invalid');
                        resolve(true);
                    } break;
                }

                
            });

        });
    }
}