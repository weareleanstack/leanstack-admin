import { LeanstackAdminPage } from './app.po';

describe('leanstack-admin App', function() {
  let page: LeanstackAdminPage;

  beforeEach(() => {
    page = new LeanstackAdminPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
